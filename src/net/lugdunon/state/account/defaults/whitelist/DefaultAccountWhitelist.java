package net.lugdunon.state.account.defaults.whitelist;

import java.util.Iterator;
import java.util.List;

import net.lugdunon.state.State;
import net.lugdunon.state.account.IAccountWhitelist;
import net.lugdunon.util.FileUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultAccountWhitelist implements IAccountWhitelist
{
	private static final Logger LOGGER         =LoggerFactory.getLogger(DefaultAccountWhitelist.class);
	private static final String WHITE_LIST_FILE="whitelist.txt";
	private List<String>        whitelist;
	private long                lastModified;
	
	@Override
	public void initWhitelist()
    {
		whitelist   =FileUtils.getLTSeparatedValues(WHITE_LIST_FILE);
		lastModified=FileUtils.lastModified        (WHITE_LIST_FILE);
    }

	@Override
	public boolean usesExternalChangePolling()
	{
		return(true);
	}
	
	@Override
    public void checkForExternalChanges()
    {
	    long lmCheck=FileUtils.lastModified(WHITE_LIST_FILE);
	    
	    if(lastModified < lmCheck)
	    {
	    	LOGGER.info("Whitelist modified externally, updating.");
	    	
	    	lastModified=lmCheck;
	    	whitelist   =FileUtils.getLTSeparatedValues(WHITE_LIST_FILE);
	    }
    }

	@Override
	public int whiteListCount()
	{
		return(whitelist.size());
	}

	@Override
	public Iterator<String> whiteListIterator()
	{
		return(whitelist.iterator());
	}

	@Override
	public boolean isWhiteList()
	{
		return(whitelist.size() > 0);
	}

	@Override
	public boolean isWhiteListed(String account)
	{
		if(State.instance().isSuperUser(account))
		{
			return(true);
		}
		
		for(String wlAcc:whitelist)
		{
			if(wlAcc.equals(account))
			{
				return(true);
			}
		}
		
		return(false);
	}

	@Override
	public void unWhiteList(String account)
    {
		if(hasExternalModifications())
		{
	    	LOGGER.info("Whitelist modified externally, updating.");
	    	
			whitelist=FileUtils.getLTSeparatedValues(WHITE_LIST_FILE);
		}
		
		whitelist.remove(account);
		
		State.instance().kickAccount(
			account,
			(String) State.instance().getWorld().getWorldConfigProperty(
				"account.whitelist.denied.message"
			)
		);
	    
	    FileUtils.writeLTSeparatedValues(WHITE_LIST_FILE,whitelist);
	    
		lastModified=FileUtils.lastModified(WHITE_LIST_FILE);
    }

	@Override
	public void whiteList(String account)
    {
		if(hasExternalModifications())
		{
	    	LOGGER.info("Whitelist modified externally, updating.");
	    	
			whitelist=FileUtils.getLTSeparatedValues(WHITE_LIST_FILE);
		}
		
		whitelist.add(account);
	    
	    FileUtils.writeLTSeparatedValues(WHITE_LIST_FILE,whitelist);
	    
		lastModified=FileUtils.lastModified(WHITE_LIST_FILE);
    }

    private boolean hasExternalModifications()
    {
	    return(lastModified < FileUtils.lastModified(WHITE_LIST_FILE));
    }
}
